<?php

namespace Drupal\social_media_content;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Language\LanguageManagerInterface;
use GuzzleHttp\Client;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\Exception\FileWriteException;

/**
 * Class ImportUtils.
 */
class ImportUtils {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;
  /**
   * Drupal\Core\Messenger\MessengerInterface definition.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal\Core\Language\LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /***
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $socialMediaContentConfig;

  /**
   * Constructs a new ImportUtils object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    Client $http_client,
    MessengerInterface $messenger,
    FileSystemInterface $file_system,
    LanguageManagerInterface $language_manager
  ) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->messenger = $messenger;
    $this->fileSystem = $file_system;
    $this->languageManager = $language_manager;
    $this->socialMediaContentConfig = $this->configFactory->get('social_media_content.settings');
  }

  /**
   * Returns a file from an external image url.
   *
   * @param string $image_url
   *
   * @return \Drupal\file\FileInterface|null
   */
  public function fileFromExternalUrl($image_url) {
    $file = NULL;
    $hash = md5($image_url);
    $localDirectory = 'public://social-media-content';
    $extension = pathinfo(parse_url($image_url, PHP_URL_PATH), PATHINFO_EXTENSION);
    $localUri = $localDirectory . '/' . $hash . '.' . $extension;
    // Requires Drupal >= 8.7, wait a bit more for BC.
    // $this->fileSystem->prepareDirectory($localDirectory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);.
    file_prepare_directory($localDirectory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    try {
      $response = $this->httpClient->get($image_url);
      if ($response->getStatusCode() === 200) {
        try {
          // Requires Drupal >= 8.7, wait a bit more for BC.
          // $file = file_save_data($response->getBody(), $localUri, FileSystemInterface::EXISTS_REPLACE);.
          $file = file_save_data($response->getBody(), $localUri, FILE_EXISTS_REPLACE);
        }
        catch (FileWriteException $exception) {
          $this->messenger->addError($this->t('The file could not be created.'));
        }
        catch (\Throwable $exception) {
          $this->messenger->addError($exception->getMessage());
        }
      }
    }
    catch (\Throwable $exception) {
      $this->messenger->addError($this->t('Could not fetch the file from the API.'));
      // Avoid to print full Guzzle exception.
      // $this->messenger->addError($exception->getMessage());
    }
    return $file;
  }

  /**
   * Returns the default author (user id) from the configuration.
   *
   * @return int
   */
  public function getDefaultAuthorId() {
    return (int) $this->socialMediaContentConfig->get('default_author');
  }

  /**
   * Returns the source language code from the configuration.
   *
   * @return string
   */
  public function getSourceLanguageCode() {
    $sourceLanguageCode = $this->socialMediaContentConfig->get('source_language');
    // The configured source language might not be available at some point
    // so fallback to the default language in this case.
    $availableLanguages = $this->languageManager->getLanguages();
    if (!array_key_exists($sourceLanguageCode, $availableLanguages)) {
      $result = $this->languageManager->getDefaultLanguage()->getId();
    }
    else {
      $result = $sourceLanguageCode;
    }
    return $result;
  }

}
