<?php


namespace Drupal\social_media_content;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\FileInterface;
use Drupal\node\Entity\Node;

/**
 * Class ImporterBase
 *
 * Importers could be converted into a Plugin system
 * so other modules might extend it.
 *
 * @package Drupal\social_media_content
 */
abstract class ImporterBase {

  use StringTranslationTrait;

  /**
   * Drupal\social_media_content\ImportUtils definition.
   *
   * @var \Drupal\social_media_content\ImportUtils
   */
  protected $importUtils;

  /**
   * ImporterBase constructor.
   */
  public function __construct() {
    $this->importUtils = \Drupal::service('social_media_content.import_utils');
  }

  /**
   * Get recent posts from a social media.
   *
   * @return array
   */
  abstract public function getRecentPosts();

  /**
   * Imports posts as content entities.
   *
   * @param array $original_posts
   */
  public function importPosts(array $original_posts) {
    if (is_array($original_posts)) {
      /** @var array $originaPost */
      // @todo use a queue worker.
      foreach ($original_posts as $originalPost) {
        $post = $this->mapPost($originalPost);
        if (!$this->postExists($post->getOrigin(), $post->getId())) {
          try {
            $entity = $this->createContent($post);
            $decoratedEntity = $this->decorateContent($entity, $post);
            $this->saveContent($decoratedEntity);
          }
          catch (\Exception $exception) {
            \Drupal::logger('social_media_content')->error($exception->getMessage());
          }
        }
      }
    }
  }

  /**
   * Checks if a post exists.
   *
   * @param string $social_media
   * @param string $post_id
   *
   * @return bool
   */
  protected function postExists($social_media, $post_id) {
    $query = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'social_media_post')
      ->condition('field_social_media_origin', $social_media)
      ->condition('field_social_media_post_id', $post_id);
    return !empty($query->execute());
  }

  /**
   * Adds social media specific contents.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   * @param \Drupal\social_media_content\PostInterface $post
   *
   * @return mixed
   */
  abstract protected function decorateContent(ContentEntityInterface $entity, PostInterface $post);

  /**
   * Saves the content entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   */
  protected function saveContent(ContentEntityInterface $entity) {
    try {
      $entity->save();
    }
    catch (\Exception $exception) {
      \Drupal::logger('social_media_content')->error($exception->getMessage());
    }
  }

  /**
   * Adapter that maps generic social media post data.
   *
   * So each social media has an interface usable by the importer.
   *
   * @param array $orginal_post
   *
   * @return \Drupal\social_media_content\PostInterface
   */
  abstract protected function mapPost(array $orginal_post);

//  public function updatePosts(array $original_posts) {
//    // @todo implement.
//    // @todo add other methods that gets posts (per post id, all posts, ...).
//  }

  /**
   * Creates a generic content entity from the social media post.
   *
   * This can be extended per social media by decorateContent(),
   * implemented on each importer.
   *
   * @param \Drupal\social_media_content\PostInterface $post
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   * @throws \Drupal\social_media_content\SocialMediaFileException
   */
  protected function createContent(PostInterface $original_post) {
    // We could have 2 approaches that may be configurable at some point.

    // 1) Use embeddable, like Media Entity (Instagram, ...)
    // https://www.drupal.org/project/media_entity_instagram

    // 2) Use fieldable, and map to a desired structure, that could be:
    // - A Node from a dedicated content type as a wrapper
    // - A Media image or image field
    // - A Media video or link field or video embed field
    // - A user reference for the user
    // - A text list or taxonomy term for the social media origin.

    // Currently implementing a basic structure for Instagram.
    // without making it too specific (e.g. video embed field for video).
    $values = [
      'type' => 'social_media_post',
      'title' => $original_post->getContentTitle(),
      'created' => $original_post->getCreatedTime(),
      'langcode' => $this->importUtils->getSourceLanguageCode(),
      'uid' => $this->importUtils->getDefaultAuthorId(),
      'field_social_media_origin' => $original_post->getOrigin(),
      'field_social_media_post_id' => $original_post->getId(),
      'field_social_media_post_link' => $original_post->getUrl(),
    ];

    if (!empty($original_post->getContentDescription())) {
      $values['field_social_media_description'] = $original_post->getContentDescription();
    };

    if (!empty($original_post->getMediaUrl())) {
      $file = $this->importUtils->fileFromExternalUrl($original_post->getMediaUrl());
      if ($file instanceof FileInterface) {
        $values['field_social_media_image'] = [
          'target_id' => $file->id(),
          'alt' => '', // @todo get caption.
        ];
      }
    }
    else {
      throw new SocialMediaFileException($this->t('The media file from @url was not accessible.', [
        '@url' => $original_post->getMediaUrl(),
      ]));
    }

    $node = Node::create($values);
    return $node;
  }

}
