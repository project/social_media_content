<?php

namespace Drupal\social_media_content;

/**
 * Interface PostInterface.
 *
 * @package Drupal\social_media_content
 */
interface PostInterface {

  // @todo set in PostBase specialisation classes, use polymorphism instead.
  const INSTAGRAM = 'instagram';

  const FACEBOOK = 'facebook';

  const TWITTER = 'twitter';

  const MEDIA_VIDEO = 'video';

  const MEDIA_IMAGE = 'image';

  const MEDIA_CAROUSEL = 'carousel';

  /**
   * Returns the post title for the content creation.
   *
   * @return string
   */
  public function getContentTitle();

  /**
   * Returns the post description for the content creation.
   *
   * @return string
   */
  public function getContentDescription();

  /**
   * @return string
   */
  public function getOrigin();

  /**
   * @param string $origin
   */
  public function setOrigin($origin);

  /**
   * @return string
   */
  public function getId();

  /**
   * @param string $id
   */
  public function setId($id);

  /**
   * @return string
   */
  public function getUrl();

  /**
   * @param string $url
   */
  public function setUrl($url);

  /**
   * @return string
   */
  public function getDescription();

  /**
   * @param string $description
   */
  public function setDescription($description);

  /**
   * @return string
   */
  public function getCreatedTime();

  /**
   * @param string $createdTime
   */
  public function setCreatedTime($createdTime);

  /**
   * @return string
   */
  public function getUserName();

  /**
   * @param string $userName
   */
  public function setUserName($userName);

  /**
   * @return string
   */
  public function getMediaType();

  /**
   * @param string $mediaType
   */
  public function setMediaType($mediaType);

  /**
   * @return string
   */
  public function getMediaUrl();

  /**
   * @param string $mediaUrl
   */
  public function setMediaUrl($mediaUrl);

  /**
   * @return array
   */
  public function getOriginalPost();

  /**
   * @param array $originalPost
   */
  public function setOriginalPost($originalPost);

}
