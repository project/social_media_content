<?php

namespace Drupal\social_media_content\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\social_media_content\InstagramImporter;

/**
 * Class InstagramController.
 *
 * On demand import.
 *
 * @todo provide form for manual import
 * @todo make it generic and use a select by social media
 */
class InstagramController extends ControllerBase {

  /**
   * Drupal\social_media_content\InstagramImporter definition.
   *
   * @var \Drupal\social_media_content\InstagramImporter
   */
  protected $instagramImporter;

  /**
   * Constructs a new InstagramController object.
   */
  public function __construct(InstagramImporter $instagram_importer) {
    $this->instagramImporter = $instagram_importer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('social_media_content.instagram_import')
    );
  }

  /**
   * Imports Instagram content.
   *
   * @return array
   *   Return render array.
   */
  public function import() {
    $originalPosts = $this->instagramImporter->getRecentPosts();
    if (!empty($originalPosts)) {
      // $originalPosts = $this->instagramImporter->getCachedPosts();
      $this->instagramImporter->importPosts($originalPosts);
    }
    return [
      '#type' => 'markup',
      '#markup' => $this->t('Imports recent Instagram posts.'),
    ];
  }

}
