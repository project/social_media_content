<?php

namespace Drupal\social_media_content\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Language\LanguageManagerInterface definition.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SettingsForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LanguageManagerInterface $language_manager,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($config_factory);
    $this->languageManager = $language_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('language_manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'social_media_content.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'social_media_content.settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_media_content.settings');

    $defaultAuthorConfig = $config->get('default_author');
    $defaultAuthor = NULL;
    if (!empty($defaultAuthorConfig)) {
      $defaultAuthor = $this->entityTypeManager->getStorage('user')->load($defaultAuthorConfig);
    }

    $defaultLanguage = $this->languageManager->getDefaultLanguage();
    if ($defaultLanguage->getId() !== $config->get('source_language')) {
      $this->messenger()->addWarning($this->t('The site default language (@default_language) differs from the source language that will be used for the nodes to be imported.', [
        '@default_language' => $defaultLanguage->getName(),
      ]));
    }

    // @todo configuration link to /admin/config/media/instagram_api

    $form['source_language'] = [
      '#type' => 'language_select',
      '#title' => $this->t('Source language'),
      '#description' => $this->t('Default language for the nodes to be created.'),
      '#default_value' => $config->get('source_language'),
      '#required' => TRUE,
    ];

    $form['default_author'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#title' => $this->t('Default author'),
      '#description' => $this->t('Default author for the nodes to be created.'),
      '#default_value' => $defaultAuthor,
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('social_media_content.settings')
      ->set('source_language', $form_state->getValue('source_language'))
      ->set('default_author', $form_state->getValue('default_author'))
      ->save();
  }

}
