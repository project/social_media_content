<?php

namespace Drupal\social_media_content;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\file\FileInterface;
use Drupal\instagram_api\Service\Client;
use Drupal\instagram_api\Service\Users;

/**
 * Class InstagramImporter.
 */
class InstagramImporter extends ImporterBase {

  /**
   * Drupal\instagram_api\Service\Client definition.
   *
   * @var \Drupal\instagram_api\Service\Client
   */
  protected $instagramApiClient;

  /**
   * Drupal\instagram_api\Service\Users definition.
   *
   * @var \Drupal\instagram_api\Service\Users
   */
  protected $instagramApiUsers;

  /**
   * Constructs a new InstagramImport object.
   */
  public function __construct(
    Client $instagram_api_client,
    Users $instagram_api_users
  ) {
    parent::__construct();
    $this->instagramApiClient = $instagram_api_client;
    $this->instagramApiUsers = $instagram_api_users;
  }

  /**
   * {@inheritdDoc}
   */
  public function getRecentPosts() {
    // Currently delegating to Instagram API, could be set
    // to another API wrapper depending on the other social media implementation.
    $result = [];
    // 20 last posts.
    $posts = $this->instagramApiUsers->getSelfMediaRecent();
    if ($posts) {
      $result = $posts;
    }
    return $result;
  }

  // Debug only,
  // @todo move in importer base with specific cache id's.
  public function getCachedPosts() {
    $result = NULL;
    $cacheId = 'instagram_posts';
    if ($cache = \Drupal::cache()->get($cacheId)) {
      $result = $cache->data;
    }
    else {
      $result = $this->getRecentPosts();
      \Drupal::cache()->set($cacheId, $result);
    }
    return $result;
  }

  /**
   * {@inheritdDoc}
   */
  protected function decorateContent(ContentEntityInterface $entity, PostInterface $post) {
    $originalPost = $post->getOriginalPost();
    switch ($post->getMediaType()) {
      case PostBase::MEDIA_VIDEO:
        // Video embed Instagram uses the post url instead of the video url.
        $entity->set('field_social_media_video_embed', $post->getUrl());
        break;
      case PostBase::MEDIA_CAROUSEL:
        $carouselImages = [];
        foreach ($originalPost['carousel_media'] as $media) {
          if (
            is_array($media['images']) &&
            array_key_exists('standard_resolution', $media['images']) &&
            array_key_exists('url', $media['images']['standard_resolution'])
          ) {
            $file = $this->importUtils->fileFromExternalUrl($media['images']['standard_resolution']['url']);
            if ($file instanceof FileInterface) {
              $carouselImages[] = [
                'target_id' => $file->id(),
                'alt' => '', // no caption per image.
              ];
            }
          }
        }
        if (!empty($carouselImages)) {
          $entity->set('field_social_media_carousel', $carouselImages);
        }
        break;
    }
    return $entity;
  }

  /**
   * {@inheritdDoc}
   */
  protected function mapPost(array $original_post) {
    $post = new PostBase();
    $post->setOrigin(PostInterface::INSTAGRAM);
    $post->setOriginalPost($original_post);
    $post->setId($original_post['id']);
    $post->setUrl($original_post['link']);
    $post->setDescription($original_post['caption']['text']);
    $post->setCreatedTime($original_post['created_time']);
    $post->setUserName($original_post['user']['username']);
    $post->setMediaType($original_post['type']);
    // Every media type has a an image, so keep it in the generic post mapping.
    if (
      is_array($original_post['images']) &&
      array_key_exists('standard_resolution', $original_post['images']) &&
      array_key_exists('url', $original_post['images']['standard_resolution'])
    ) {
      $post->setMediaUrl($original_post['images']['standard_resolution']['url']);
    }
    return $post;
  }

}
