# Social Media Content

Imports recent social media posts as content entities.

## Use cases

When the Drupal ecosystem is used instead of embeds:
posts using fields, images styles, theme layer, Views query 
(e.g. sort social media content by date along with other content).

When a facade is needed to expose several social media sources.  

## Dependencies

- https://www.drupal.org/project/instagram_api
- https://www.drupal.org/project/video_embed_field
- https://www.drupal.org/project/video_embed_instagram

## Configuration

- Configure the Instagram API credentials (/admin/config/media/instagram_api)
- Content is imported when the cron runs.

## Related modules

- https://www.drupal.org/project/social_feed_aggregator
- https://www.drupal.org/project/socialfeed
- https://www.drupal.org/project/social_migration
- https://www.drupal.org/project/social_timeline

### Difference with other modules.

Makes use of the post creation date for the Drupal content creation date.

Imports images locally.

For Instagram, imports a generic image for all post types and also imports
- Video as a _Video Embed Instagram_ field
- Carousel as a multiple image field.

@todo explain other differences.

## Roadmap

- Only the Instagram importer is implemented, to come: Twitter, Facebook and Mastodon
- Implement import as a queue worker
- Implement update and other import methods (by post id, ...)
- Small improvements like: manual import, title, configurable cron, logging, ...
- Provide a field formatter for the social media source
- Map social media user to Drupal user if the relation exists
- Import user details (picture, site, ...) or configure user for import
