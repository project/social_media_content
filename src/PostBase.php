<?php

namespace Drupal\social_media_content;

use Drupal\Component\Utility\Xss;

/**
 * Class PostBase.
 *
 * PostBase value object that provides a facade
 * for different social media post.
 *
 * @package Drupal\social_media_content
 */
class PostBase implements PostInterface {

  /**
   * @var string
   */
  private $origin;

  /**
   * @var string
   */
  private $id;

  /**
   * @var string
   */
  private $url;

  /**
   * @var string
   */
  private $description;

  /**
   * @var string
   */
  private $createdTime;

  /**
   * @var string
   */
  private $userName;

  /**
   * @var string
   */
  private $mediaType;

  /**
   * @var string
   */
  private $mediaUrl;

  /**
   * @var array
   */
  private $originalPost;

  /**
   * {@inheritDoc}.
   */
  public function getContentTitle() {
    // @todo improve with the description, and post time instead of the id.
    $result = ucfirst($this->getOrigin()) . ' ' . $this->getId();
    // If (!is_null($this->getDescription())) {.
    // @todo truncate utf8, strip tags
    // }.
    return $result;
  }

  /**
   * {@inheritDoc}.
   */
  public function getContentDescription() {
    $result = '';
    if (!is_null($this->description)) {
      $result = Xss::filter($this->description);
    }
    return $result;
  }

  /**
   * {@inheritDoc}.
   */
  public function getOrigin() {
    return $this->origin;
  }

  /**
   * {@inheritDoc}.
   */
  public function setOrigin($origin) {
    $this->origin = $origin;
  }

  /**
   * {@inheritDoc}.
   */
  public function getId() {
    return $this->id;
  }

  /**
   * {@inheritDoc}.
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * {@inheritDoc}.
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * {@inheritDoc}.
   */
  public function setUrl($url) {
    $this->url = $url;
  }

  /**
   * {@inheritDoc}.
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritDoc}.
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * {@inheritDoc}.
   */
  public function getCreatedTime() {
    // @todo make sure to convert it into the right format (timestamp).
    return $this->createdTime;
  }

  /**
   * {@inheritDoc}.
   */
  public function setCreatedTime($createdTime) {
    $this->createdTime = $createdTime;
  }

  /**
   * {@inheritDoc}.
   */
  public function getUserName() {
    return $this->userName;
  }

  /**
   * {@inheritDoc}.
   */
  public function setUserName($userName) {
    $this->userName = $userName;
  }

  /**
   * {@inheritDoc}.
   */
  public function getMediaType() {
    return $this->mediaType;
  }

  /**
   * {@inheritDoc}.
   */
  public function setMediaType($mediaType) {
    $this->mediaType = $mediaType;
  }

  /**
   * {@inheritDoc}.
   */
  public function getMediaUrl() {
    return $this->mediaUrl;
  }

  /**
   * {@inheritDoc}.
   */
  public function setMediaUrl($mediaUrl) {
    $this->mediaUrl = $mediaUrl;
  }

  /**
   * {@inheritDoc}.
   */
  public function getOriginalPost() {
    return $this->originalPost;
  }

  /**
   * {@inheritDoc}.
   */
  public function setOriginalPost($originalPost) {
    $this->originalPost = $originalPost;
  }

}
